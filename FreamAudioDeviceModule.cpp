#include "FreamAudioDeviceModule.h"

#include <talk/base/thread.h>

FreamAudioDeviceModule::FreamAudioDeviceModule() :
		transport(0)
{
}

FreamAudioDeviceModule::~FreamAudioDeviceModule()
{
}

int32_t FreamAudioDeviceModule::ActiveAudioLayer(AudioLayer *audioLayer) const
{
	return 0;
}

int32_t FreamAudioDeviceModule::AddRef()
{
	return 0;
}

webrtc::AudioDeviceModule::ErrorCode FreamAudioDeviceModule::LastError() const
{
	return kAdmErrNone;
}

int32_t FreamAudioDeviceModule::RegisterEventObserver(webrtc::AudioDeviceObserver *eventCallback)
{
	return 0;
}

int32_t FreamAudioDeviceModule::RegisterAudioCallback(webrtc::AudioTransport *audioCallback)
{
	transport = audioCallback;

	play_thread = new talk_base::Thread();
	play_thread->Start(this);

	return 0;
}

int32_t FreamAudioDeviceModule::Init()
{
	return 0;
}

int32_t FreamAudioDeviceModule::Terminate()
{
	return 0;
}

bool FreamAudioDeviceModule::Initialized() const
{
	return true;
}

int32_t FreamAudioDeviceModule::StopRawOutputFileRecording()
{
	return 0;
}

int16_t FreamAudioDeviceModule::PlayoutDevices()
{
	return 0;
}

int16_t FreamAudioDeviceModule::RecordingDevices()
{
	return 0;
}

int32_t FreamAudioDeviceModule::PlayoutDeviceName(uint16_t index, char name[webrtc::kAdmMaxDeviceNameSize], char guid[webrtc::kAdmMaxGuidSize])
{
	return 0;
}

int32_t FreamAudioDeviceModule::RecordingDeviceName(uint16_t index, char name[webrtc::kAdmMaxDeviceNameSize], char guid[webrtc::kAdmMaxGuidSize])
{
	return 0;
}

int32_t FreamAudioDeviceModule::SetPlayoutDevice(uint16_t index)
{
	return 0;
}

int32_t FreamAudioDeviceModule::SetPlayoutDevice(WindowsDeviceType device)
{
	return 0;
}

int32_t FreamAudioDeviceModule::SetRecordingDevice(uint16_t index)
{
	return 0;
}

int32_t FreamAudioDeviceModule::SetRecordingDevice(WindowsDeviceType device)
{
	return 0;
}

int32_t FreamAudioDeviceModule::PlayoutIsAvailable(bool *available)
{
	*available = false;

	return 0;
}

int32_t FreamAudioDeviceModule::InitPlayout()
{
	return 0;
}

bool FreamAudioDeviceModule::PlayoutIsInitialized() const
{
	return false;
}

int32_t FreamAudioDeviceModule::RecordingIsAvailable(bool *available)
{
	*available = false;

	return 0;
}

int32_t FreamAudioDeviceModule::InitRecording()
{
	return 0;
}

bool FreamAudioDeviceModule::RecordingIsInitialized() const
{
	return false;
}

int32_t FreamAudioDeviceModule::StartPlayout()
{
	return 0;
}

int32_t FreamAudioDeviceModule::StopPlayout()
{
	return 0;
}

bool FreamAudioDeviceModule::Playing() const
{
	return true;
}

int32_t FreamAudioDeviceModule::StartRecording()
{
	return 0;
}

int32_t FreamAudioDeviceModule::StopRecording()
{
	return 0;
}

bool FreamAudioDeviceModule::Recording() const
{
	return 0;
}

int32_t FreamAudioDeviceModule::SetAGC(bool enable)
{
	return 0;
}

bool FreamAudioDeviceModule::AGC() const
{
	return true;
}

int32_t FreamAudioDeviceModule::SetWaveOutVolume(uint16_t volumeLeft, uint16_t volumeRight)
{
	return 0;
}

int32_t FreamAudioDeviceModule::WaveOutVolume(uint16_t *volumeLeft, uint16_t *volumeRight) const
{
	*volumeLeft = 0;
	*volumeRight = 0;

	return 0;
}

int32_t FreamAudioDeviceModule::SpeakerIsAvailable(bool *available)
{
	*available = true;

	return 0;
}

int32_t FreamAudioDeviceModule::InitSpeaker()
{
	return 0;
}

bool FreamAudioDeviceModule::SpeakerIsInitialized() const
{
	return true;
}

int32_t FreamAudioDeviceModule::MicrophoneIsAvailable(bool *available)
{
	*available = false;

	return 0;
}

int32_t FreamAudioDeviceModule::InitMicrophone()
{
	return 0;
}

bool FreamAudioDeviceModule::MicrophoneIsInitialized() const
{
	return true;
}

int32_t FreamAudioDeviceModule::SpeakerVolumeIsAvailable(bool *available)
{
	*available = false;

	return 0;
}

int32_t FreamAudioDeviceModule::SetSpeakerVolume(uint32_t volume)
{
	return 0;
}

int32_t FreamAudioDeviceModule::SpeakerVolume(uint32_t *volume) const
{
	*volume = 0;

	return 0;
}

int32_t FreamAudioDeviceModule::MaxSpeakerVolume(uint32_t *maxVolume) const
{
	*maxVolume = 0;

	return 0;
}

int32_t FreamAudioDeviceModule::MinSpeakerVolume(uint32_t *minVolume) const
{
	*minVolume = 0;

	return 0;
}

int32_t FreamAudioDeviceModule::SpeakerVolumeStepSize(uint16_t *stepSize) const
{
	*stepSize = 0;

	return 0;
}

int32_t FreamAudioDeviceModule::MicrophoneVolumeIsAvailable(bool *available)
{
	*available = false;

	return 0;
}

int32_t FreamAudioDeviceModule::SetMicrophoneVolume(uint32_t volume)
{
	return 0;
}

int32_t FreamAudioDeviceModule::MicrophoneVolume(uint32_t *volume) const
{
	*volume = 0;

	return 0;
}

int32_t FreamAudioDeviceModule::MaxMicrophoneVolume(uint32_t *maxVolume) const
{
	*maxVolume = 0;

	return 0;
}

int32_t FreamAudioDeviceModule::MinMicrophoneVolume(uint32_t *minVolume) const
{
	*minVolume = 0;

	return 0;
}

int32_t FreamAudioDeviceModule::MicrophoneVolumeStepSize(uint16_t *stepSize) const
{
	*stepSize = 0;

	return 0;
}

int32_t FreamAudioDeviceModule::SpeakerMuteIsAvailable(bool *available)
{
	*available = false;

	return 0;
}

int32_t FreamAudioDeviceModule::SetSpeakerMute(bool enable)
{
	return 0;
}

int32_t FreamAudioDeviceModule::SpeakerMute(bool *enabled) const
{
	*enabled = false;

	return 0;
}

int32_t FreamAudioDeviceModule::MicrophoneMuteIsAvailable(bool *available)
{
	*available = false;

	return 0;
}

int32_t FreamAudioDeviceModule::SetMicrophoneMute(bool enable)
{
	return 0;
}

int32_t FreamAudioDeviceModule::MicrophoneMute(bool *enabled) const
{
	*enabled = false;

	return 0;
}

int32_t FreamAudioDeviceModule::MicrophoneBoostIsAvailable(bool *available)
{
	*available = false;

	return 0;
}

int32_t FreamAudioDeviceModule::SetMicrophoneBoost(bool enable)
{
	return 0;
}

int32_t FreamAudioDeviceModule::MicrophoneBoost(bool *enabled) const
{
	*enabled = false;

	return 0;
}

int32_t FreamAudioDeviceModule::StereoPlayoutIsAvailable(bool *available) const
{
	*available = true;

	return 0;
}

int32_t FreamAudioDeviceModule::SetStereoPlayout(bool enable)
{
	return 0;
}

int32_t FreamAudioDeviceModule::StereoPlayout(bool *enabled) const
{
	return 0;
}

int32_t FreamAudioDeviceModule::StereoRecordingIsAvailable(bool *available) const
{
	*available = true;

	return 0;
}

int32_t FreamAudioDeviceModule::SetStereoRecording(bool enable)
{
	return 0;
}

int32_t FreamAudioDeviceModule::StereoRecording(bool *enabled) const
{
	*enabled = true;

	return 0;
}

int32_t FreamAudioDeviceModule::SetRecordingChannel(const ChannelType channel)
{
	return 0;
}

int32_t FreamAudioDeviceModule::RecordingChannel(ChannelType *channel) const
{
	return 0;
}

int32_t FreamAudioDeviceModule::SetPlayoutBuffer(const BufferType type, uint16_t sizeMS)
{
	return 0;
}

int32_t FreamAudioDeviceModule::PlayoutBuffer(BufferType *type, uint16_t *sizeMS) const
{
	return 0;
}

int32_t FreamAudioDeviceModule::PlayoutDelay(uint16_t *delayMS) const
{
	*delayMS = 0;

	return 0;
}

int32_t FreamAudioDeviceModule::RecordingDelay(uint16_t *delayMS) const
{
	*delayMS = 0;

	return 0;
}

int32_t FreamAudioDeviceModule::CPULoad(uint16_t *load) const
{
	*load = 0;

	return 0;
}

int32_t FreamAudioDeviceModule::StartRawOutputFileRecording(const char pcmFileNameUTF8[webrtc::kAdmMaxFileNameSize])
{
	return 0;
}

int32_t FreamAudioDeviceModule::StartRawInputFileRecording(const char pcmFileNameUTF8[webrtc::kAdmMaxFileNameSize])
{
	return 0;
}

int32_t FreamAudioDeviceModule::StopRawInputFileRecording()
{
	return 0;
}

int32_t FreamAudioDeviceModule::SetRecordingSampleRate(const uint32_t samplesPerSec)
{
	return 0;
}

int32_t FreamAudioDeviceModule::RecordingSampleRate(uint32_t *samplesPerSec) const
{
	*samplesPerSec = 0;

	return 0;
}

int32_t FreamAudioDeviceModule::SetPlayoutSampleRate(const uint32_t samplesPerSec)
{
	return 0;
}

int32_t FreamAudioDeviceModule::PlayoutSampleRate(uint32_t *samplesPerSec) const
{
	*samplesPerSec = 44100;

	return 0;
}

int32_t FreamAudioDeviceModule::ResetAudioDevice()
{
	return 0;
}

int32_t FreamAudioDeviceModule::SetLoudspeakerStatus(bool enable)
{
	return 0;
}

int32_t FreamAudioDeviceModule::GetLoudspeakerStatus(bool *enabled) const
{
	enabled = false;

	return 0;
}

int32_t FreamAudioDeviceModule::TimeUntilNextProcess()
{
	return 0;
}

int32_t FreamAudioDeviceModule::Process()
{
	return 0;
}

void FreamAudioDeviceModule::Run(talk_base::Thread * running_thread)
{
	int8_t * playBuffer = new int8_t[3840];
	uint32_t nSamplesOut(0);

	for (;;)
	{
		transport->NeedMorePlayData(960, 4, 2, 96000, playBuffer, nSamplesOut);
		running_thread->SleepMs(10);
	}
}

