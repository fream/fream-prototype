#ifndef FREAM_H_
#define FREAM_H_

#include "FreamVideoRenderer.h"

#include <talk/app/webrtc/peerconnection.h>
#include <talk/app/webrtc/mediastream.h>

#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>

#include <ogg/ogg.h>

class Fream: public webrtc::PeerConnectionObserver
{
public:
	Fream(const boost::filesystem::path & output, const boost::filesystem::path & input, const boost::filesystem::path & outputVideoPath);
	virtual ~Fream();

	// Implement PeerConnectionObserver
	virtual void OnError();
	virtual void OnMessage(const std::string& msg);
	virtual void OnSignalingMessage(const std::string& msg);
	virtual void OnStateChange(StateType state_changed);
	virtual void OnAddStream(webrtc::MediaStreamInterface* stream);
	virtual void OnRemoveStream(webrtc::MediaStreamInterface* stream);
	virtual void OnIceCandidate(const webrtc::IceCandidateInterface* candidate);
	virtual void OnIceComplete();

private:
	boost::filesystem::path m_output;
	boost::filesystem::ofstream m_videoOutput;
	talk_base::scoped_refptr<webrtc::VideoRendererWrapperInterface> m_videoRenderer;

	void sendMessage(const std::string& msg);
	void writePage(ogg_page page);

	friend class FreamVideoRenderer;
};

#endif /* FREAM_H_ */
