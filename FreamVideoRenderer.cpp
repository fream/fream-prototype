#include "FreamVideoRenderer.h"
#include "Fream.h"

#include <talk/session/phone/videoframe.h>

#include <ogg/ogg.h>
#include <theora/theoraenc.h>

#include <cstdlib>
#include <ctime>

FreamVideoRenderer::FreamVideoRenderer(Fream & app) :
		m_app(app), m_targetFPS(25), m_lastTimeStamp(0), m_lastVideoFrame(NULL)
{
	m_context = NULL;
	srand((unsigned int)time(NULL));
	ogg_stream_init(&m_videoStream, rand());
}

FreamVideoRenderer::~FreamVideoRenderer()
{
	th_encode_free(m_context);
	ogg_stream_destroy(&m_videoStream);
}

bool FreamVideoRenderer::SetSize(int width, int height, int reserved)
{
	th_info info;
	th_comment videoComment;
	ogg_packet videoHeaderPacket;
	ogg_page nextHeaderPage;

	if (m_context)
	{
		//TODO Is this even valid? How should we handle resize?
		th_encode_free(m_context);
	}

	th_info_init(&info);

	info.frame_width = 16 * ((width + 15) / 16);
	info.frame_height = 16 * ((height + 15) / 16);
	info.pic_x = 0;
	info.pic_y = 0;
	info.pic_width = width;
	info.pic_height = height;
	info.pixel_fmt = TH_PF_420;
	info.fps_numerator = m_targetFPS;
	info.fps_denominator = 1;
	info.colorspace = TH_CS_UNSPECIFIED;
	info.quality = 63;

	th_comment_init(&videoComment);
	th_comment_add_tag(&videoComment, const_cast<char *>("Author"), const_cast<char *>("Created by Fream"));

	m_context = th_encode_alloc(&info);

	while (th_encode_flushheader(m_context, &videoComment, &videoHeaderPacket))
	{
		ogg_stream_packetin(&m_videoStream, &videoHeaderPacket);

		while (ogg_stream_pageout(&m_videoStream, &nextHeaderPage))
			m_app.writePage(nextHeaderPage);
	}

	ogg_stream_flush(&m_videoStream, &nextHeaderPage);
	m_app.writePage(nextHeaderPage);

	th_comment_clear(&videoComment);
	th_info_clear(&info);

	return true;
}

bool FreamVideoRenderer::RenderFrame(const cricket::VideoFrame * frame)
{
	int64 currentTimeStamp = frame->GetTimeStamp();

	if (m_lastTimeStamp > 0)
	{
		int64 missingFrames = (currentTimeStamp - m_lastTimeStamp) / (1000000000 / m_targetFPS) - 1;

		if (missingFrames > -1)
		{
			for (int64 i = 0; i < missingFrames; i++)
				RenderFrameInternal(m_lastVideoFrame.get());

			RenderFrameInternal(frame);
		}
	}
	else
		RenderFrameInternal(frame);

	return true;
}

void FreamVideoRenderer::RenderFrameInternal(const cricket::VideoFrame * frame)
{
	th_ycbcr_buffer buffer;
	ogg_packet videoPacket;
	ogg_page nextPage;
	int encodingResult;

	if (frame->GetTimeStamp() != m_lastTimeStamp)
	{
		m_lastVideoFrame.reset(frame->Copy());
		m_lastTimeStamp = frame->GetTimeStamp();
	}

	buffer[0].data = const_cast<uint8*>(frame->GetYPlane());
	buffer[0].stride = frame->GetYPitch();
	buffer[0].width = frame->GetWidth();
	buffer[0].height = frame->GetHeight();

	buffer[1].data = const_cast<uint8*>(frame->GetUPlane());
	buffer[1].stride = frame->GetUPitch();
	buffer[1].width = frame->GetChromaWidth();
	buffer[1].height = frame->GetChromaHeight();

	buffer[2].data = const_cast<uint8*>(frame->GetVPlane());
	buffer[2].stride = frame->GetVPitch();
	buffer[2].width = frame->GetChromaWidth();
	buffer[2].height = frame->GetChromaHeight();

	encodingResult = th_encode_ycbcr_in(m_context, buffer);

	if (encodingResult)
	{
		switch (encodingResult)
		{
		case TH_EFAULT:
			std::cerr << "Context is null" << std::endl;
			break;
		case TH_EINVAL:
			std::cerr << "Size is invalid" << std::endl;
			break;
		}
	}

	while (th_encode_packetout(m_context, 0, &videoPacket))
	{
		ogg_stream_packetin(&m_videoStream, &videoPacket);

		while (ogg_stream_pageout(&m_videoStream, &nextPage))
			m_app.writePage(nextPage);
	}

	ogg_stream_flush(&m_videoStream, &nextPage);
	m_app.writePage(nextPage);
}
