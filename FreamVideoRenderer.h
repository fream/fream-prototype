#ifndef FREAMVIDEORENDERER_H_
#define FREAMVIDEORENDERER_H_

#include <talk/session/phone/videorenderer.h>

#include <theora/theoraenc.h>
#include <memory>

class Fream;

class FreamVideoRenderer: public cricket::VideoRenderer
{
public:
	FreamVideoRenderer(Fream & app);
	virtual ~FreamVideoRenderer();

	bool SetSize(int width, int height, int reserved);
	bool RenderFrame(const cricket::VideoFrame * frame);

private:
	Fream & m_app;
	th_enc_ctx * m_context;
	ogg_stream_state m_videoStream;
	int m_targetFPS;
	long long m_lastTimeStamp;
	std::auto_ptr<cricket::VideoFrame> m_lastVideoFrame;

	void RenderFrameInternal(const cricket::VideoFrame * frame);
};

#endif /* FREAMVIDEORENDERER_H_ */
