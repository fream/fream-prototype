#include "Fream.h"

#include <iostream>
#include <string>
#include <cerrno>
#include <cstring>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>

// Taken from http://www.zedwood.com/article/111/cpp-urlencode-function
// TODO Replace with framework code
std::string char2hex(char dec)
{
	char dig1 = (dec & 0xF0) >> 4;
	char dig2 = (dec & 0x0F);
	if (0 <= dig1 && dig1 <= 9)
		dig1 += 48; //0,48inascii
	if (10 <= dig1 && dig1 <= 15)
		dig1 += 97 - 10; //a,97inascii
	if (0 <= dig2 && dig2 <= 9)
		dig2 += 48;
	if (10 <= dig2 && dig2 <= 15)
		dig2 += 97 - 10;

	std::string r;
	r.append(&dig1, 1);
	r.append(&dig2, 1);
	return r;
}

// Taken from http://www.zedwood.com/article/111/cpp-urlencode-function
// TODO Replace with framework code
std::string urlencode(const std::string &c)
{

	std::string escaped = "";
	int max = c.length();
	for (int i = 0; i < max; i++)
	{
		if ((48 <= c[i] && c[i] <= 57) || //0-9
				(65 <= c[i] && c[i] <= 90) || //abc...xyz
				(97 <= c[i] && c[i] <= 122) || //ABC...XYZ
				(c[i] == '~' || c[i] == '!' || c[i] == '*' || c[i] == '(' || c[i] == ')' || c[i] == '\''))
		{
			escaped.append(&c[i], 1);
		}
		else
		{
			escaped.append("%");
			escaped.append(char2hex(c[i])); //converts char 255 to string "ff"
		}
	}
	return escaped;
}

Fream::Fream(const boost::filesystem::path & output, const boost::filesystem::path & input, const boost::filesystem::path & outputVideoPath) :
		m_output(output), m_videoOutput(outputVideoPath)
{
	sendMessage(input.filename().string());
	m_videoRenderer = webrtc::CreateVideoRenderer(new FreamVideoRenderer(*this));
}

Fream::~Fream()
{
}

void Fream::OnError()
{
	std::cerr << "Error occurred";
}

void Fream::OnMessage(const std::string & msg)
{
	sendMessage(msg);
}

void Fream::OnSignalingMessage(const std::string & msg)
{
	sendMessage("SDP\n" + msg);
}

void Fream::OnStateChange(StateType state_changed)
{
}

void Fream::OnAddStream(webrtc::MediaStreamInterface *stream)
{
	stream->video_tracks()->at(0)->SetRenderer(m_videoRenderer);
}

void Fream::OnRemoveStream(webrtc::MediaStreamInterface *stream)
{
}

void Fream::OnIceCandidate(const webrtc::IceCandidateInterface *candidate)
{
}

void Fream::OnIceComplete()
{
}

void Fream::sendMessage(const std::string & msg)
{
	std::string realMsg = urlencode(msg);
	int fifofd = open(m_output.c_str(), O_WRONLY);

	if (write(fifofd, realMsg.c_str(), realMsg.length()) == -1)
		std::cerr << std::strerror(errno) << std::endl;

	close(fifofd);
}

void Fream::writePage(ogg_page page)
{
	m_videoOutput.write(reinterpret_cast<char *>(page.header), page.header_len);
	m_videoOutput.write(reinterpret_cast<char *>(page.body), page.body_len);
	m_videoOutput.flush();
}

