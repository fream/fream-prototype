/**
 * Signaling for PeerConnection, using anonymous FIFOs on server.
 * 
 * Copyright Ericsson AB, 2011. The software may only be used as specified by
 * the copyright holder.
 * 
 * THE SOFTWARE IS PROVIDED “AS IS”. ERICSSON MAKES NO REPRESENTATIONS OR
 * WARRANTIES WITH RESPECT TO THE SOFTWARE,WHETHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE. SPECIFICALLY, WITHOUT LIMITING THE GENERALITY OF THE
 * FOREGOING, ERICSSON MAKES NO REPRESENTATION OR WARRANTY THAT (I) THE USE OF
 * THE SOFTWARE AND OR PARTS THEREOF WILL BE UNINTERRUPTED OR ERRORFREE, AND OR
 * (II) ANY REPRODUCTION, USE OF THE SOFTWARE AND OR PARTS THEREOF ARE FREE FROM
 * INFRINGEMENT OF ANY INTELLECTUAL PROPERTY RIGHTS AND IT SHALL BE THE SOLE
 * RESPONSIBILITY OF THE USER TO MAKE SUCH DETERMINATION AS IS NECESSARY WITH
 * RESPECT TO THE ACQUISITION OF LICENSES. CONSEQUENTLY, IN NO EVENT SHALL
 * ERICSSON BE LIABLE TO ANY PARTY FOR ANY LOSS OR DAMAGES (WHETHER DIRECT,
 * INDIRECT OR CONSEQUENTIAL) ARISING FROM ANY USE, REPRODUCTION AND OR
 * DISTRIBUTION OF THE SOFTWARE AND OR ANY PARTS THEREOF.
 */

var Signaling = {

	fifoId : null, // fifo for receiving signaling messages
	remoteFifoId : null, // fifo for sending signaling messages
	Peer : null, // PeerConnection instance
	eventSrc : null,

	TURN_CONFIG : "STUN stun.l.google.com:19302",

	FIFO_STATE_NONE : 0, // FIFO not yet created
	FIFO_STATE_CREATED : 1, // Initial state upon creation, expecting acceptance
	FIFO_STATE_ACCEPTED : 2, // Received OOB acceptance message from remote
	// peer, PeerConnection connecting
	fifo_state : 0,

	// ===== Initial handshake

	// create(): inviter receives a FIFO id from the server
	create : function(on_url_created) {
		var createClient = new XMLHttpRequest();
		createClient.open("GET", "create.php?" + Signaling.fifoId, true);
		createClient.send(null);
		createClient.onreadystatechange = function() {
			if (this.readyState === 4 && this.status === 200) {
				Signaling.fifoId = this.responseText.replace(/^\s*/, "")
						.replace(/\s*$/, "");
				Signaling.fifo_state = Signaling.FIFO_STATE_CREATED;
				Signaling.eventSrc = new EventSource("recv.php?id="
						+ Signaling.fifoId);
				Signaling.eventSrc.addEventListener("message",
						Signaling.onSigMsg);
				console.log("listening to " + Signaling.fifoId);

				var url_base = location.href.substring(0, location.href
						.lastIndexOf('/'));
				on_url_created(
						url_base + "/accept.html?id=" + Signaling.fifoId,
						Signaling.fifoId);
			}
		};
	},

	// accept(): store remote FIFO ID, pass local FIFO ID to remote peer
	accept : function(remoteFifoId) {
		Signaling.remoteFifoId = remoteFifoId;
		Signaling.sendSigMsg("" + Signaling.fifoId);

		try {
			Signaling.Peer = new webkitPeerConnection(Signaling.TURN_CONFIG,
					Signaling.sendSigMsg);
		} catch (e) {
			Signaling.Peer = new webkitDeprecatedPeerConnection(
					Signaling.TURN_CONFIG, Signaling.sendSigMsg);
		}

		Signaling.Peer.onconnecting = Signaling.onconnectingInternal;
		Signaling.Peer.onopen = Signaling.onopenInternal;

		Signaling.fifo_state = Signaling.FIFO_STATE_ACCEPTED;
	},

	// ===== Sending messages

	// sendSigMsg() sends a msg to the server via XHR
	sendSigMsg : function(msg) {
		console.log("sending to " + Signaling.remoteFifoId);
		console.log(msg);
		var sendClient = new XMLHttpRequest();
		sendClient.onreadystatechange = function() {
		};
		sendClient.open("POST", "post.php", true);
		sendClient.setRequestHeader("Content-Type",
				"application/x-www-form-urlencoded");
		sendClient.send("id=" + Signaling.remoteFifoId + "&msg="
				+ encodeURIComponent(msg), true);
	},

	// ===== Receiving messages

	// onSigMsg is invoked when receiving event/signaling message
	// from the server. It pushes the signaling message down to
	// PeerConnection.
	onSigMsg : function(event) {
		console.log("incoming message!");
		var msg = decodeURIComponent(event.data);
		console.log(msg);
		switch (Signaling.fifo_state) {
		case Signaling.FIFO_STATE_CREATED:
			// The inviter just received its first message,
			// containing the other peer's FIFO ID. Now we
			// create a PeerConnection lazily, to minimize the
			// probability of ICE time-outs.
			Signaling.remoteFifoId = msg;
			Signaling.fifo_state = Signaling.FIFO_STATE_ACCEPTED;

			try {
				Signaling.Peer = new webkitPeerConnection(
						Signaling.TURN_CONFIG, Signaling.sendSigMsg);
			} catch (e) {
				Signaling.Peer = new webkitDeprecatedPeerConnection(
						Signaling.TURN_CONFIG, Signaling.sendSigMsg);
			}

			Signaling.Peer.onconnecting = Signaling.onconnectingInternal;
			Signaling.Peer.onopen = Signaling.onopenInternal;
			break;

		case Signaling.FIFO_STATE_ACCEPTED:
			Signaling.Peer.processSignalingMessage(msg);
			break;

		case Signaling.FIFO_STATE_NONE:
		default:
			console.log("Error: got message in state #" + Signaling.fifo_state);
			break;
		}
	},

	// proxy methods for onconnecting()/onopen()

	onconnectingInternal : function() {
		if (Signaling.onconnecting) {
			Signaling.onconnecting();
		}
	},

	onopenInternal : function() {
		if (Signaling.onopen) {
			Signaling.onopen();
		}
	},

}; // end of Signaling

