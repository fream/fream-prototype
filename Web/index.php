<?php

include_once("config.php");

$streaming = file_exists($workdir.$outfile) && file_exists($ogvfile);

?>

<html>
<head>
<link href="chat.css" rel="stylesheet" type="text/css" />








<?php if(!$streaming): ?>
<script type="text/javascript" src="signaling.js"></script>
<script>
	var content;
	
	window.onload = function() {
		content = document.getElementById('content');
		selfView = document.getElementById('selfView');

		Signaling.onconnecting = function() {
			navigator.webkitGetUserMedia({audio: true, video: true}, function(stream) {
				selfView.muted = true;
				selfView.src = webkitURL.createObjectURL(stream);
				Signaling.Peer.addStream(stream);
			},  onUserMediaError);
		}

		Signaling.create(function(url, fifoId) {});
	}

	var addButton;

	//Review if we still need this
	function requestSelfVideo() {
		navigator.webkitGetUserMedia({audio: true, video: true}, function(stream) {
			selfView.muted = true;
			selfView.src = webkitURL.createObjectURL(stream);
			Signaling.Peer.addStream(stream);
		},  onUserMediaError);
		
		addButton.disabled = "true";
	}

	function receiveVideoStream(e) {
		peerView.src = webkitURL.createObjectURL(e.stream);
	}

	function onUserMediaError(error) {
		console.log("Failed to get access to local media. Error code was " + error.code);
		alert("Failed to get access to local media. Error code was " + error.code + ".");
	}
</script>
<?php endif; ?>
</head>
<body>

	<div id="content">
		<h1>Fream Prototype</h1>
		<table align=center>
			<tr height="240">
				<td width="320"><video id="selfView" width="320" height="240"





				<?php if($streaming): ?>
						src="http://localhost/<?php echo $ogvurl; ?>"


							
						<?php endif; ?> autoplay controls>
					</video>
				</td>
			</tr>
		</table>
		
		
		
		
	  <?php if(!$streaming): ?>
<p>You are now streaming. Open a new broswer and navigate to this URL to watch the stream.</p>
	  <?php else: ?>
<p>You are watching a sample stream in Fream.</p>
	  <?php endif; ?>
	</div>
</body>
</html>
