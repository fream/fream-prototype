<?php

// recv.php: receives server-sent events containing signaling messages
//
// Copyright Ericsson AB, 2011. The software may only be used as
// specified by the copyright holder.
//
// THE SOFTWARE IS PROVIDED “AS IS”. ERICSSON MAKES NO REPRESENTATIONS
// OR WARRANTIES WITH RESPECT TO THE SOFTWARE,WHETHER EXPRESS OR IMPLIED,
// INCLUDING BUT NOT LIMITED TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE. SPECIFICALLY, WITHOUT LIMITING THE
// GENERALITY OF THE FOREGOING, ERICSSON MAKES NO REPRESENTATION OR WARRANTY
// THAT (I) THE USE OF THE SOFTWARE AND OR PARTS THEREOF WILL BE
// UNINTERRUPTED OR ERRORFREE, AND OR (II) ANY REPRODUCTION, USE OF THE
// SOFTWARE AND OR PARTS THEREOF ARE FREE FROM INFRINGEMENT OF ANY
// INTELLECTUAL PROPERTY RIGHTS AND IT SHALL BE THE SOLE RESPONSIBILITY
// OF THE USER TO MAKE SUCH DETERMINATION AS IS NECESSARY WITH RESPECT TO
// THE ACQUISITION OF LICENSES. CONSEQUENTLY, IN NO EVENT SHALL ERICSSON
// BE LIABLE TO ANY PARTY FOR ANY LOSS OR DAMAGES (WHETHER DIRECT, INDIRECT
// OR CONSEQUENTIAL) ARISING FROM ANY USE, REPRODUCTION AND OR DISTRIBUTION
// OF THE SOFTWARE AND OR ANY PARTS THEREOF.


header("Content-Type: text/event-stream");
header("Cache-Control: no-cache");


function print_line($fd, $events, $args)
{
	echo "data: ".fgets($fd);
}

if (strpos($_GET["id"], "/")) die("invalid FIFO");
$filename = "/tmp/fream/".$_GET["id"];

// see note below
// register_shutdown_function("delete", $filename);

$fd = fopen($filename, "r");
if (! $fd) die("cannot read FIFO");

$base = event_base_new();
$event = event_new();

// NOTE: this should really have the EV_PERSIST flag merged in,
// but PHP (or Apache, or something else) seems to buffer things
// a bit too much, and the browser ends up getting nothing. This
// solution is inefficient, but works. However, we lose the
// opportunity to remove the FIFO, so the number of FIFOs will
// grow over time, and they will need to be cleaned out somehow.

// event_set($event, $fd, EV_WRITE | EV_PERSIST, "print_line");
event_set($event, $fd, EV_WRITE, "print_line");

event_base_set($event, $base);
event_add($event);
event_base_loop($base);
?>







