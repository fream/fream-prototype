#include "Fream.h"
#include "FreamAudioDeviceModule.h"

#include <talk/app/webrtc/peerconnection.h>
#include <talk/app/webrtc/portallocatorfactory.h>
#include <talk/base/scoped_ptr.h>

#include <iostream>
#include <fstream>
#include <string>

#include <boost/filesystem.hpp>

#include <event2/event.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>

void fifocb(evutil_socket_t fd, short flags, void * args)
{
	talk_base::scoped_refptr<webrtc::PeerConnectionInterface> pconn(reinterpret_cast<webrtc::PeerConnectionInterface *>(args));
	const int buffsize = 256;
	char buffer[buffsize];
	std::string message;
	size_t jsonStart; //TODO Just aim to chop the first line

	memset(buffer, '\0', buffsize);

	while (read(fd, buffer, buffsize - 1) > 0)
	{
		message += buffer;
		memset(buffer, '\0', buffsize);
	}

	if ((jsonStart = message.find('{')) != std::string::npos)
		message = message.substr(jsonStart);

	pconn->ProcessSignalingMessage(message);
}

int main(int argc, char ** argv)
{
	if (argc >= 5)
	{
		boost::filesystem::path workingDir(argv[1]);
		boost::filesystem::path outputFile(workingDir / argv[2]);
		boost::filesystem::path inputFile(workingDir / argv[3]);
		boost::filesystem::path outputVideoPath(argv[4]);
		int fifofd = mkfifo(inputFile.c_str(), S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IWOTH | S_IROTH);

		close(fifofd);
		fifofd = open(inputFile.c_str(), O_RDWR | O_NONBLOCK);

		Fream app(outputFile, inputFile, outputVideoPath);

		talk_base::Thread worker_thread;
		worker_thread.Start();

		talk_base::Thread signaling_thread;
		signaling_thread.Start();

		talk_base::scoped_refptr<webrtc::PortAllocatorFactoryInterface> port_factory = webrtc::PortAllocatorFactory::Create(&worker_thread);
		FreamAudioDeviceModule fream_mod;

		talk_base::scoped_refptr<webrtc::PeerConnectionFactoryInterface> factory = webrtc::CreatePeerConnectionFactory(&worker_thread, &signaling_thread, port_factory.get(), &fream_mod);
		talk_base::scoped_refptr<webrtc::PeerConnectionInterface> pconn = factory->CreateRoapPeerConnection("STUN stun.l.google.com:19302", &app);

		event_base * signaling = event_base_new();
		event * fifoevent = event_new(signaling, fifofd, EV_READ | EV_PERSIST, fifocb, pconn.get());

		event_add(fifoevent, NULL);
		event_base_dispatch(signaling);

		return 0;
	}

	return 1;
}
